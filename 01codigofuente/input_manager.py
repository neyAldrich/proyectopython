from vector import *

class InputManager:

    def __init__(self):

        #Diccionario (int = bool)
        self.keymap = {}
        self.previous_keymap = {}

        #Vec2
        self.mouseCoords = Vec2(0.0, 0.0)
#METODOS

    def update(self):
        for key in self.keymap.keys():
            self.previous_keymap[key] = self.keymap[key]

    def pressKey(self, keyID):
        self.keymap[keyID] = True

    def releaseKey(self, keyID):
        self.keymap[keyID] = False

    def setMouseCoords(self, x, y):
        self.mouseCoords.x = x
        self.mouseCoords.y = y


    def isKeyDown(self, keyID):

        if keyID in self.keymap:
            return self.keymap[keyID]
        else:
            return False

    def isKeyPressed(self, keyID):

        #DEBUG
        # print("Pressed key " + str(keyID))

        return (self.isKeyDown(keyID) is True and self.wasKeyDown(keyID) is False)

    def wasKeyDown(self, keyID):
        if keyID in self.previous_keymap:
            return self.previous_keymap[keyID]
        else:
            return False
