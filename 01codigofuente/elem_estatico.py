import pygame
# pygame.init()

import colores
from constantes import *
from proyecto_error import *
from vector import *

class ElementoEstatico:
    """Define elementos colisionables sin movimiento"""

    def __init__(self, pos):

        #Vec2
        self.posicion = pos

        #Sprite
        self.sprite = None
        self.converted = False

        #Color
        self.color = colores.BLANCO

    def getSprite(self):
        if not self.converted:
            try:
                self.sprite.convert_alpha()
            except pygame.error:
                pass
            else:
                self.converted = True

        self.sprite = self.sprite = pygame.transform.scale(self.sprite, (ANCHO_ELEM_ESTATICO, ANCHO_ELEM_ESTATICO))

        return self.sprite


class Coco(ElementoEstatico):

    def __init__(self, pos, tipo):
        super().__init__(pos)

        #Enum TIPO_COCO
        self.tipo = tipo

        if tipo is TIPO_COCO.NORMAL:
            self.sprite = pygame.image.load("Texturas/coco.png")
        elif tipo is TIPO_COCO.SUPER:
            self.sprite = pygame.image.load("Texturas/superCoco.png")
        else:
            pygame.quit()
            crash("Error en el tipo de Coco, terminando ejecucion")



class Fruta(ElementoEstatico):

    def __init__(self, pos, tipo):
        super().__init__(pos)
        self.puntuacion = 0

        #Enum TIPO_FRUTA
        self.tipo = tipo

        if tipo is TIPO_FRUTA.CEREZA:
            self.sprite = pygame.image.load("Texturas/cereza.png")
            self.puntuacion = 100
        elif tipo is TIPO_FRUTA.FRESA:
            self.sprite = pygame.image.load("Texturas/fresa.png")
            self.puntuacion = 500
        else:
            pygame.quit()
            crash("Error en el tipo de Fruta, terminando ejecucion")