import pygame
# pygame.init()

from constantes import *
from vector import *
import colores

class Nivel:
    """ Permite leer un nivel desde archivo """

    def __init__(self):

        #Lista de strings
        self.nivel_data = []

        #Entero
        self.numCocos = 0


        #Posiciones de elementos

        #Vec2
        self.posicionPacman = None
        self.posFruta = None

        #Lista de Vec2
        self.posicionesCocos = []
        self.posicionesSuperCocos = []

        #Diccionario (TIPO_FANTASMA = POSICION)
        self.posicionesFantasmas = {}


        #Lista de instancias de Puerta
        self.puertas = []
        self.posSalida = None

        #Atajos

        #Vec2
        self.posAtajo1 = None
        self.posAtajo2 = None


        #Lista de elementos a renderizar
        self.spriteBatch = []

        #Superficie a retornar
        self.surface = pygame.Surface((ANCHO_VENTANA, ALTO_VENTANA))
        # self.surface = self.surface.convert_alpha()


#--------------------
#METODOS

# Carga un nivel desde archivo, leyendo caracter a caracter y asignando los parametros
#    de forma adecuada
# Creada en 26/06/2015
# Autor Neycker Aguayo
# Version 1.0
    def cargarNivel(self, dir_archivo):

        archivo = open(dir_archivo, 'r')

        #Eliminar cabecera
        for i in range(0, CABECERA_NIVEL):
            tmp = archivo.readline()

        linea = archivo.readline()
        linea = linea[:-1]

        while linea is not "":
            #Separo los strings en caracteres
            self.nivel_data = self.nivel_data + [list(linea)]
            linea = archivo.readline()
            if linea[-2:-1] is "\n":
                linea = linea[:-1]

        #Renderizar todas las baldosas
        for y in range(0, len(self.nivel_data)):
            for x in range(0, len(self.nivel_data[y])):

                #Se toma la baldosa
                baldosa = self.nivel_data[y][x]

                #Muros del nivel
                if baldosa is 'M':
                    pygame.draw.rect(self.surface, colores.AZUL,
                                     (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS, ANCHO_BALDOSAS, ANCHO_BALDOSAS))
                elif baldosa is 'X':
                    img = pygame.image.load("Texturas/glass.png")
                    img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))

                    self.surface.blit(img, (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS))
                    self.nivel_data[y][x] = 'M'
                elif baldosa is 'Z':
                    img = pygame.image.load("Texturas/red_bricks.png")
                    img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))

                    self.surface.blit(img, (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS))
                    self.nivel_data[y][x] = 'M'
                elif baldosa is 'V':
                    img = pygame.image.load("Texturas/light_bricks.png")
                    img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))

                    self.surface.blit(img, (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS))
                    self.nivel_data[y][x] = 'M'

                #Pared de Prision
                elif baldosa is 'P':
                    pygame.draw.rect(self.surface, colores.AZUL,
                                     (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS, ANCHO_BALDOSAS, ANCHO_BALDOSAS))
                    self.nivel_data[y][x] = 'M'
                elif baldosa is 'E':
                    self.posSalida = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)
                    self.nivel_data[y][x] = '.'
                elif baldosa is 'D':
                    img = pygame.image.load("Texturas/puertaPrision.png")
                    img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))

                    self.surface.blit(img, (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS))
                    self.nivel_data[y][x] = 'M'
                elif baldosa is 'd':
                    img = pygame.image.load("Texturas/puertaPrision.png")
                    img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))
                    img = pygame.transform.rotate(img, 90)

                    self.surface.blit(img, (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS))
                    self.nivel_data[y][x] = 'M'

                #Cocos
                elif baldosa is '.':
                    self.posicionesCocos += [Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)]
                    self.numCocos += 1
                elif baldosa is 'S':
                    self.posicionesSuperCocos += [Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)]
                    self.numCocos += 1

                #Atajos
                elif baldosa is '1':
                    pygame.draw.rect(self.surface, colores.NEGRO,
                                     (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS, ANCHO_BALDOSAS, ANCHO_BALDOSAS))

                    self.posAtajo1 = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)

                elif baldosa is '2':
                    pygame.draw.rect(self.surface, colores.NEGRO,
                                     (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS, ANCHO_BALDOSAS, ANCHO_BALDOSAS))

                    self.posAtajo2 = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)
                elif baldosa is '<':
                    pygame.draw.rect(self.surface, colores.NEGRO,
                                    (x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS, ANCHO_BALDOSAS, ANCHO_BALDOSAS))
                    self.nivel_data[y][x] = 'M'

                #Fruta
                elif baldosa is 'f':
                    self.posFruta = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)

                #Fantasmas
                elif baldosa is 'F': #FANTASMA ROJO
                    self.posicionesFantasmas[TIPO_FANTASMA.ROJO] = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)
                    self.nivel_data[y][x] = '.'
                elif baldosa is 'R': #FANTASMA ROSA
                    self.posicionesFantasmas[TIPO_FANTASMA.ROSA] = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)
                    self.nivel_data[y][x] = '.'
                elif baldosa is 'C': #FANTASMA CELESTE
                    self.posicionesFantasmas[TIPO_FANTASMA.CELESTE] = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)
                    self.nivel_data[y][x] = '.'
                elif baldosa is 'A': #FANTASMA AMARILLO
                    self.posicionesFantasmas[TIPO_FANTASMA.AMARILLO] = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)
                    self.nivel_data[y][x] = '.'

                #Pacman
                elif baldosa is '@':
                    self.posicionPacman = Vec2(x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS)
                    self.nivel_data[y][x] = '.'
                else:
                    # print('Simbolo no reconocido')
                    pass

#Dibujar nivel
    def dibujar(self, game_surface):
        game_surface.blit(self.surface, (0, 0))