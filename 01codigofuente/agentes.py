import pygame
# pygame.init()

import  random
import time
import colores
from constantes import *
from proyecto_error import *
from vector import *
from Engine.texture_manager import *

import math
import Juego
import nivel


class Agente:

    def __init__(self):

        #Vec2 int
        self.posicion = None
        self.direccion = None

        #Vec2 float
        self.velocidad = None

        #Sprite
        self.sprite = None
        self.textura = None
        self.converted = False

        self.textureManager = None

        self.fps_counter = 0

        #Color
        self.color = None

#METODOS

    def getSprite(self):
        if not self.converted:
            try:
                self.textura.convert_alpha()
            except pygame.error:
                pass
            else:
                self.converted = True
        return self.textura

    #Animacion
    def setTextura(self, texture_path):
        self.textureManager.getImage(self.textureManager.getId(texture_path))


    def setDireccion(self, enum_direccion):
        dir = None
        if enum_direccion is DIRECCION.ARRIBA:
            dir = Vec2(0.0, -1.0)
        if enum_direccion is DIRECCION.ABAJO:
            dir = Vec2(0.0, 1.0)
        if enum_direccion is DIRECCION.IZQUIERDA:
            dir = Vec2(-1.0, 0.0)
        if enum_direccion is DIRECCION.DERECHA:
            dir = Vec2(1.0, 0.0)

        return dir

    #Colisiones

    def colisionarConNivel(self, nivel_data):

        alguna_colision = False

        for y in range(0, len(nivel_data)):
            for x in range(0, len(nivel_data[y])):

                if nivel_data[y][x] == 'M':
                    rectMuro = pygame.Rect((x * ANCHO_BALDOSAS, y * ANCHO_BALDOSAS), (ANCHO_BALDOSAS, ANCHO_BALDOSAS))

                    rectAgente = pygame.Rect(self.posicion.toTuple(), (ANCHO_AGENTE, ANCHO_AGENTE))

                    colisiona = True

                    colisiona = rectAgente.colliderect(rectMuro)

                    if colisiona:
                        # print("Colisiona con muro")
                        self.colisionarConBaldosa(Vec2.fromTuple(rectMuro.center))
                        alguna_colision = True

        return alguna_colision

    def colisionarConAgente(self, agente):
        DISTANCIA_MINIMA = RADIO_AGENTE * 1.5
        #Vec2.__init__(RADIO_AGENTE,RADIO_AGENTE)
        centerPosA = Vec2(self.posicion.x + RADIO_AGENTE, self.posicion.y + RADIO_AGENTE)
        centerPosB = Vec2(agente.posicion.x + RADIO_AGENTE, agente.posicion.y + RADIO_AGENTE)

        distVec = centerPosA - centerPosB
        distancia = Vec2.length(distVec)
        collisionDepth = DISTANCIA_MINIMA - distancia
        if collisionDepth > 0:
            collisionDepthVec = Vec2.normalize(distVec) * collisionDepth
            self.posicion += Vec2(collisionDepthVec.x / 2.0, collisionDepthVec.y / 2.0)
            agente.posicion -= Vec2(collisionDepthVec.x / 2.0, collisionDepthVec.y / 2.0)
            #self.posicion += collisionDepthVec/2.0
            #agente.posicion -= collisionDepthVec / 2.0
            return True
        return False

    #Internos
    def colisionarConBaldosa(self, pos_baldosa):

        DISTANCIA_MINIMA = RADIO_BALDOSA + RADIO_AGENTE

        centro_agente = Vec2(self.posicion.x + RADIO_AGENTE, self.posicion.y + RADIO_AGENTE)
        dist_vec = centro_agente - pos_baldosa

        xDepth = DISTANCIA_MINIMA - abs(dist_vec.x)
        yDepth = DISTANCIA_MINIMA - abs(dist_vec.y)

        if xDepth > 0 or yDepth > 0:
            if max(xDepth, 0.0) < max(yDepth, 0.0):
                if dist_vec.x < 0:
                    self.posicion.x -= xDepth
                else:
                    self.posicion.x += xDepth
            else:
                if dist_vec.y < 0:
                    self.posicion.y -= yDepth
                else:
                    self.posicion.y += yDepth

    # def verificarPosBaldosa(self, nivel_data, posBaldosasAColisionar, x, y):
        # pass

    def colisionarConPuerta(self, Puerta):
        pass

    def valoresMuyCercanos(self, x, y, delta):
        return (abs(x - y) < delta)

    def generarDireccion(self):

        r2 = [1, 2, 3, 4]
        random.shuffle(r2)

        for i in range(0, len(r2)):
             j = r2[i]

             if DIRECCION.ABAJO.value is j:
                return DIRECCION.ABAJO
             elif DIRECCION.ARRIBA.value is j:
                return DIRECCION.ARRIBA
             elif DIRECCION.DERECHA.value is j:
                return DIRECCION.DERECHA
             elif DIRECCION.IZQUIERDA.value is j:
                return DIRECCION.IZQUIERDA



    #Virtual
    def actualizar(self, nivel):
        pass

    #Virtual
    def dibujar(self, game_surface):
        pass

    def getPos(self):
        assert self.posicion is not None
        return self.posicion


class Pacman(Agente):

    def __init__(self):
        super().__init__()

        #Bool
        self.poder_activo = None

        #Int
        self.vidas = 3
        self.tiempo_restante_poder = None  #En Ticks

        #Vec2
        self.oldpos = None
        self.olddir = None
        self.nextdir = None

        self.posInicial = None

        #Angulo de rotacion de sprite
        self.angle = None

        #Sprite extra de animacion
        self.textura2 = None
        self.current_textura = None

        #InputManager
        self.inputManager = None

#METODOS

    def iniciar(self, velocidad, posicion, inputManager, textureManager):
        self.velocidad = velocidad
        self.posicion = posicion
        self.direccion = self.setDireccion(DIRECCION.IZQUIERDA)

        self.oldpos = self.posicion
        self.olddir = self.direccion
        self.nextdir = self.direccion

        self.poder_activo = False
        self.tiempo_restante_poder = 0

        self.inputManager = inputManager
        self.textureManager = textureManager

        self.current_textura = 0

        self.textura = pygame.image.load("Texturas/pacman1.png")
        self.textura = pygame.transform.scale(self.textura, (ANCHO_AGENTE, ANCHO_AGENTE))
        self.textura = self.textura.convert_alpha()

        self.textura2 = pygame.image.load("Texturas/pacman2.png")
        self.textura2 = pygame.transform.scale(self.textura2, (ANCHO_AGENTE, ANCHO_AGENTE))
        self.textura2 = self.textura2.convert_alpha()

    def getSprite(self):
        assert self.textura is not None
        assert self.textura2 is not None

        if not self.converted:
            try:
                self.textura.convert_alpha()
            except pygame.error:
                pass
            else:
                self.converted = True

        if self.current_textura == 1:
            return pygame.transform.rotate(self.textura2, self.angle)

        return pygame.transform.rotate(self.textura, self.angle)


    def actualizar(self, nivel):

        self.olddir = self.direccion
        self.oldpos = self.posicion

        if self.inputManager.isKeyPressed(pygame.K_w):
            self.nextdir = self.setDireccion(DIRECCION.ARRIBA)
        elif self.inputManager.isKeyPressed(pygame.K_s):
            self.nextdir = self.setDireccion(DIRECCION.ABAJO)
        elif self.inputManager.isKeyPressed(pygame.K_a):
            self.nextdir = self.setDireccion(DIRECCION.IZQUIERDA)
        elif self.inputManager.isKeyPressed(pygame.K_d):
            self.nextdir = self.setDireccion(DIRECCION.DERECHA)
        elif self.inputManager.isKeyPressed(pygame.K_p):
            pass

        self.direccion = self.nextdir
        self.posicion += self.direccion * self.velocidad

        if self.colisionarConNivel(nivel.nivel_data):
            # print("POS: ", self.posicion)
            # print("NEXT DIR:", self.nextdir)
            # print("DIR: ",self.direccion)
            # print("Old dir: ", self.olddir)
            self.direccion = self.olddir
            # print("DIR: ",self.direccion)
            self.posicion += self.direccion * self.velocidad

            # print("NEW POS: ", self.posicion)
            # self.direccion = self.nexdir

        self.colisionarConNivel(nivel.nivel_data)


        #Atajos
        if self.posicion == nivel.posAtajo1:
            self.posicion = nivel.posAtajo2
            self.posicion += self.direccion * self.velocidad
        elif self.posicion == nivel.posAtajo2:
            self.posicion = nivel.posAtajo1
            self.posicion = nivel.posAtajo1


        #Calcular angulo de rotacion
        derecha = Vec2(1, 0)
        ndir = self.direccion.normalize()
        self.angle = math.acos(ndir.dot(derecha))
        self.angle = self.angle *(360/ (math.pi*2))

        if self.direccion.y > 0:
            self.angle *= -1

    def colisionarConElemento(self, elem):

        DISTANCIA_MINIMA = RADIO_ELEM_ESTATICO

        center_a = self.posicion + Vec2.fromSingle(RADIO_AGENTE)
        center_b = elem.posicion + Vec2.fromSingle(RADIO_ELEM_ESTATICO)

        dist_vec = center_a - center_b
        distancia = dist_vec.length()

        collision_depth = DISTANCIA_MINIMA - distancia

        if collision_depth > 0:
            return True
        else:
            return False


    def animacion(self):
        if self.current_textura == 0:
            self.current_textura = 1
        elif self.current_textura == 1:
            self.current_textura = 0
        else:
            print("PACMAN: current_textura con valor no valido : ", self.current_textura)


    #Poderes
    def activarPoder(self, duracion):
        self.poder_activo = True
        self.tiempo_restante_poder = duracion

    def modificarTiempoPoder(self, delta):
        self.tiempo_restante_poder += delta

        if self.tiempo_restante_poder <= 0:
            self.poder_activo = False

class Fantasma(Agente):

    def __init__(self):
        super().__init__()

        #float
        self.deltaCerca = 10 * ANCHO_BALDOSAS

        #Tupla (2 enteros)
        self.obtenerDireccionesHaciaElemento = None
        self.oldPosicion = None
        self.oldDireccion = None
        self.nextDireccion = None

        #Prision
        self.boolColisionPuerta = None
        self.boolEnPrision = None
        self.tiempoRestantePrision = None   #En Ticks

        #Tipo Fantasma
        self.tipoFantasma = None

        #Textura vulnerable
        self.textura_vulnerable = None
        self.current_textura = None

        self.boolPersigue = False

    def iniciar(self, posicion, tFantasma):

        self.posicionInicial = posicion
        self.posicion = self.posicionInicial
        self.tipoFantasma = tFantasma
        self.boolColisionPuerta = True

        self.boolEnPrision = True
        self.boolPersigue = False

        self.current_textura = 0

        if self.tipoFantasma is TIPO_FANTASMA.ROJO:
            texturaF = "Texturas/fantasmaRojo.png"
            self.direccion = self.setDireccion(DIRECCION.IZQUIERDA)
            self.velocidad = 3.5
            self.tiempoRestantePrision = 6 * FPS
            self.boolPersigue = True
        elif self.tipoFantasma is TIPO_FANTASMA.AMARILLO:
            texturaF = "Texturas/fantasmaAmarillo.png"
            self.direccion = self.setDireccion(DIRECCION.DERECHA)
            self.velocidad = 3.0
            self.tiempoRestantePrision = 5 * FPS
        elif self.tipoFantasma is TIPO_FANTASMA.CELESTE:
            texturaF = "Texturas/fantasmaCeleste.png"
            self.direccion = self.setDireccion(DIRECCION.ARRIBA)
            self.velocidad = 2.5
            self.tiempoRestantePrision = 4 * FPS
            self.boolPersigue = True
        elif self.tipoFantasma is TIPO_FANTASMA.ROSA:
            texturaF = "Texturas/fantasmaRosa.png"
            self.direccion = self.setDireccion(DIRECCION.DERECHA)
            self.velocidad = 2.0
            self.tiempoRestantePrision = 3 * FPS
        else:
            pygame.quit()
            crash("Error, fantasma no encontrado")

        self.textura = pygame.image.load(texturaF)
        self.textura = pygame.transform.scale(self.textura, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))

        self.textura_vulnerable = pygame.image.load("Texturas/fantasmaV1.png")
        self.textura_vulnerable = pygame.transform.scale(self.textura_vulnerable, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))


    def encarcelar(self, duracion):
        self.boolEnPrision = True
        self.tiempoRestantePrision = duracion
        self.posicion = self.posicionInicial

    def modificarTiempoPrision(self, delta, level):
        self.tiempoRestantePrision += delta

        if self.tiempoRestantePrision <= 0:
            self.boolEnPrision = False
            self.posicion = level.posSalida



    def actualizar(self, nivel):

        if not self.boolEnPrision:
            self.fps_counter += 1
            assert self.direccion is not None

            if self.fps_counter % 15 == 0:
                self.direccion = self.setDireccion(self.generarDireccion())

            self.posicion += self.direccion * self.velocidad
            self.colisionarConNivel(nivel.nivel_data)

            if self.posicion == nivel.posAtajo1:
                self.posicion = nivel.posAtajo2
                self.posicion += self.direccion * self.velocidad
            elif self.posicion == nivel.posAtajo2:
                self.posicion = nivel.posAtajo1
                self.posicion = nivel.posAtajo1

            if self.fps_counter == 60:
                    self.fps_counter = 0
        else:
           self.colisionarConNivel(nivel.nivel_data)

    def estaPacmanCerca(self, pacman, delta):
        vecDif = self.posicion - pacman.posicion
        return abs(vecDif.x) < delta and abs(vecDif.y) < delta

    def buscarPacman(self, pacman):
        self.oldPosicion = self.posicion
        self.oldDireccion = self.direccion

        vecDireccion = Vec2.fromSingle(0)

        if self.estaPacmanCerca(pacman, self.deltaCerca):

            try:
                vecDireccion = Vec2.normalize(pacman.posicion - self.posicion)
            except ZeroDivisionError:
                pass
            self.direccion = vecDireccion

            if pacman.poder_activo:
                self.posicion -= self.direccion * (self.velocidad/2)
            else:
                self.posicion += self.direccion * (self.velocidad/2)


        if self.valoresMuyCercanos(self.posicion.x, self.oldPosicion.x, self.velocidad) and self.valoresMuyCercanos(self.posicion.y, self.oldPosicion.y,self.velocidad):


            if self.valoresMuyCercanos(self.posicion.x, self.oldPosicion.x, self.velocidad) and self.valoresMuyCercanos(self.posicion.y, self.oldPosicion.y,  self.velocidad):
                self.direccion = vecDireccion
                self.posicion += self.direccion * (self.velocidad/2)

            if self.valoresMuyCercanos(self.posicion.x, self.oldPosicion.x, self.velocidad) and self.valoresMuyCercanos(self.posicion.y, self.oldPosicion.y,self.velocidad):

                self.nextDireccion = self.direccion
                self.direccion = self.oldDireccion
                self.posicion += self.direccion * (self.velocidad/2)

    def setTexturaVulnerable(self):
        self.current_textura = 1

    def setTexturaInicial(self):
        self.current_textura = 0

    def getSprite(self):
        if not self.converted:
            try:
                self.textura.convert_alpha()
            except pygame.error:
                pass
            else:
                self.converted = True

        if self.current_textura == 1:
            return self.textura_vulnerable

        return self.textura
