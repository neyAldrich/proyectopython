import math

class Vec2:
    """Implementacion de un vector en R2"""

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    @classmethod
    def fromSingle(cls, x=0):
        return cls(x, x)

    @classmethod
    def fromTuple(cls, tupla):
        return cls(tupla[0], tupla[1])

    #Operaciones
    def __sub__(self, other):
        return Vec2(self.x - other.x, self.y - other.y)

    def __isub__(self, other):
        return self.__sub__(other)

    def __add__(self, other):
        return Vec2(self.x + other.x, self.y + other.y)

    def __iadd__(self, other):
        return self.__add__(other)

    def __str__(self):
        return str(self.x) + ' , ' + str(self.y)

    def __mul__(self, other):
        return Vec2(self.x * other, self.y * other)

    def __imul__(self, other):
        return self.__mul__(other)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def length(self):
        r = math.sqrt(self.x**2 + self.y**2)
        return r

    def normalize(self):
        try:
            v = Vec2(self.x / self.length(), self.y / self.length())
        except ZeroDivisionError:
            return -1

        return v

    def dot(self, other):
        return self.x * other.x + self.y * other.y

    #Auxiliares
    def toTuple(self):
        return (self.x, self.y)


