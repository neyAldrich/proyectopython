import pygame
pygame.init()

import nivel
import elem_estatico
import agentes

from constantes import *
from colores import *
from vector import *
from input_manager import *
from Engine.texture_manager import *

class Juego():
    """Controlador del juego"""

    def __init__(self):

        self.fantasmas = []

    #Datos del juego
        #Instancia de Pacman
        self.pacman = agentes.Pacman()

        #Instancias de Nivel
        self.niveles = []
        self.nivel_actual = 0
        self.niveles_path = ["nivel1.txt", "nivel2.txt", "nivel3.txt"]

        #Instancias de cocos
        self.cocos = []

        #Instancia de fruta
        self.fruta = None
        self.tipos_fruta = [TIPO_FRUTA.CEREZA, TIPO_FRUTA.FRESA, TIPO_FRUTA.CEREZA]

        self.cocos_comidos = 0
        self.puntuacion = 0

        self.dificultad = 1

        #Reloj y controlador de FPS
        self.reloj = None
        self.fps_counter = 0
        self.tiempo = 0

        #DisplaySurface
        self.Ventana = None

        #Fuente texto
        self.BasicFont = pygame.font.Font('freesansbold.ttf', 16)
        self.hudFont = pygame.font.Font("Fonts/stocky/stocky.ttf", 16)
        # self.hudFont1 = pygame.font.Font("Fonts/sixty/sixty.ttf", 16)

        #Managers

        self.inputManager = InputManager()
        self.textureManager = TextureManager()

        #Audio
        self.soundCocoComido  = None
        self.soundFrutaComida = None
        self.soundFantasmaComido = None
        self.soundPacmanMuerto = None
        self.soundVidaExtra = None
        self.soundFantasmaVulnerable = None

        self.intro_played = False

        self.pause = True
#METODOS

    def arrancar(self):

        self.pantallaInicio()
        self.iniciarSistemas()

        #Niveles
        for file in self.niveles_path:
            lvl = nivel.Nivel()
            lvl.cargarNivel("Niveles/" + file)
            self.niveles += [lvl]

        # for y in range(0, len(lvl.nivelData)):
        #     for x in range(0, len(lvl.nivelData[y])):
        #         print(lvl.nivelData[y][x])

        self.iniciarNiveles()

        self.lazoJuego()

#--------------------
#Private

    def lazoJuego(self):

        salir = False

        while not salir:

            self.fps_counter += 1

            self.procesarInput()

            self.actualizarAgentes()


            #Perdida del juego
            if self.pacman.vidas <= 0:
                print("HA PERDIDO!")
                break

            #Iniciar Fruta
            if self.cocos_comidos == self.niveles[self.nivel_actual].numCocos // 5:
                self.fruta = elem_estatico.Fruta(self.niveles[self.nivel_actual].posFruta,
                                                 self.tipos_fruta[self.nivel_actual])


            self.dibujarJuego()

            self.dibujarHUD()

            pygame.display.update()


            #Cambio de nivel
            if self.cocos_comidos == self.niveles[self.nivel_actual].numCocos:
                #Ganar el juego
                if self.nivel_actual == len(self.niveles) -1:
                    print("HA GANADO!")
                    self.win()
                    break
                else:
                    self.dificultad *= 1.5

                    self.descargarNivel()
                    self.nivel_actual += 1
                    self.iniciarNiveles()

            # if not self.intro_played:
            #     pygame.mixer.music.play()
            #     self.intro_played = True
            #     pygame.time.delay(4000)

            if self.fps_counter == 60:
                self.fps_counter = 0
                self.tiempo += 1

            self.reloj.tick(60)

#Control de estados
    def salirJuego(self):
        pygame.quit()
        quit()

    def unpause(self):
        self.pause = False

        if self.pacman.poder_activo:
            pygame.mixer.music.load("Sound/new_siren.wav")
            pygame.mixer.music.play()

    def paused(self):

        # self.Ventana.fill(NEGRO)

        pygame.mixer.music.stop()

        largeText = pygame.font.SysFont("freesansbold.ttf",115)
        TextSurf, TextRect = self.text_objects("Paused", largeText)
        TextRect.center = ((ANCHO_VENTANA/2),(ALTO_VENTANA/2))
        self.Ventana.blit(TextSurf, TextRect)


        while self.pause:
            for event in pygame.event.get():
                #print(event)
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()

            self.button("Continue",150,450,100,50,BRIGHTBLUE,AZUL,self.unpause)
            self.button("Quit",550,450,100,50,BRIGHTRED,ROJO,self.salirJuego)

            pygame.display.update()
            self.reloj.tick(15)


#Iniciar Sitemas

# Inicializa los recursos principales del juego.
# Creada en 26/06/2015
# Autor Neycker Aguayo
# Version 1.0
    def iniciarSistemas(self):

        #iniciar Reloj
        self.reloj = pygame.time.Clock()

        #Iniciar sonido
        pygame.mixer.init()
        pygame.mixer.music.load("Sound/pacman_beginning.wav")

        self.soundCocoComido = pygame.mixer.Sound("Sound/new_chomp.wav")
        self.soundFrutaComida = pygame.mixer.Sound("Sound/pacman_eatfruit.wav")
        self.soundFantasmaVulnerable = pygame.mixer.Sound("Sound/pacman_intermission.wav")
        self.soundFantasmaComido = pygame.mixer.Sound("Sound/pacman_eatghost.wav")
        self.soundPacmanMuerto = pygame.mixer.Sound("Sound/pacman_death.wav")
        self.soundVidaExtra = pygame.mixer.Sound("Sound/pacman_extrapac.wav")

        self.soundFantasmaVulnerable = None #Se inicializa cuando se come el superCoco

        #Iniciar la ventana del juego
        self.Ventana = pygame.display.set_mode(
            (ANCHO_VENTANA, ALTO_VENTANA))
        # self.Ventana = self.Ventana.convert_alpha()

        pygame.display.set_caption('Pacman 2.0')
        self.Ventana.fill(NEGRO)




#Iniciar Niveles

# Inicializa los elementos de un nivel.
# Creada en 26/06/2015
# Autor Neycker Aguayo
# Version 1.0
    def iniciarNiveles(self):
        #Iniciar Pacman
        self.pacman.iniciar(VELOCIDAD_PACMAN, self.niveles[self.nivel_actual].posicionPacman, self.inputManager, self.textureManager)

        #Iniciar Fantasmas
        l = self.niveles[self.nivel_actual].posicionesFantasmas.keys()
        for key in l:
            f = agentes.Fantasma()
            f.iniciar(self.niveles[self.nivel_actual].posicionesFantasmas[key],key)
            self.fantasmas += [f]

        #Iniciar Cocos
        #Normales
        for pos in self.niveles[self.nivel_actual].posicionesCocos:
            self.cocos += [elem_estatico.Coco(pos, TIPO_COCO.NORMAL)]

        #Super Cocos
        for pos in self.niveles[self.nivel_actual].posicionesSuperCocos:
            self.cocos += [elem_estatico.Coco(pos, TIPO_COCO.SUPER)]


    def descargarNivel(self):
        for coco in self.cocos:
            del(coco)

        self.cocos = []
        self.cocos_comidos = 0

        for f in self.fantasmas:
            del(f)

        self.fantasmas = []

        if self.fruta != None:
            del(self.fruta)
            self.fruta = None


#Control de Input

    def procesarInput(self):

        for e in pygame.event.get():
            if e.type is pygame.QUIT:
                self.salirJuego()
            elif e.type is pygame.KEYDOWN:
                self.inputManager.pressKey(e.key)
                if e.key == pygame.K_p:
                    self.pause = True
                    self.paused()
                elif e.key == pygame.K_ESCAPE:
                    self.salirJuego()
                    quit()
            elif e.type is pygame.KEYUP:
                self.inputManager.releaseKey(e.key)
            elif e.type is pygame.MOUSEBUTTONDOWN:
                for click in pygame.mouse.get_pressed():
                    if click is 1:
                        self.inputManager.pressKey(click)
            elif e.type is pygame.MOUSEBUTTONUP:
                for click in pygame.mouse.get_pressed():
                    if click is 0:
                        self.inputManager.releaseKey(click)

#Actualizar Agentes
    def actualizarAgentes(self):
        #Pacman
        self.pacman.actualizar(self.niveles[self.nivel_actual])

        if self.fps_counter % 10 == 0:
            self.pacman.animacion()

        #Colision fantasmas - escenario
        for i in range(0, len(self.fantasmas)):
            if self.fantasmas[i].boolPersigue:
                self.fantasmas[i].buscarPacman(self.pacman)
            self.fantasmas[i].actualizar(self.niveles[self.nivel_actual])

        #Colision pacman - fantasmas
        for f in self.fantasmas:
            if self.pacman.colisionarConAgente(f) and self.pacman.vidas > 0:

                if not self.pacman.poder_activo:

                    self.pacman.posicion = self.niveles[self.nivel_actual].posicionPacman
                    self.pacman.vidas -= 1

                    self.soundPacmanMuerto.play()

                    for g in self.fantasmas:
                        g.posicion = self.niveles[self.nivel_actual].posicionesFantasmas[g.tipoFantasma]
                        g.encarcelar(3 * FPS)

                else:
                    tmpPos = self.niveles[self.nivel_actual].posicionesFantasmas[f.tipoFantasma]
                    f.posicion = tmpPos
                    f.encarcelar(3 * FPS)

                    self.puntuacion += PUNTUACION_FANTASMS
                    self.soundFantasmaComido.play()
            elif self.pacman.vidas == 0:
                 self.lost()


        #Colision pacman con fruta
        if self.fruta != None:
            if self.pacman.colisionarConElemento(self.fruta):
                self.puntuacion += self.fruta.puntuacion
                self.soundFrutaComida.play()

                del(self.fruta)
                self.fruta = None

        #Colision pacman con cocos
        for i in range(0, len(self.cocos)):
            if self.pacman.colisionarConElemento(self.cocos[i]):

                if pygame.mixer.get_busy() == False:
                    self.soundCocoComido.play()

                if self.cocos[i].tipo == TIPO_COCO.SUPER:
                    pygame.mixer.music.load("Sound/new_siren.wav")
                    pygame.mixer.music.play(-1)

                    if self.pacman.poder_activo:
                        self.pacman.modificarTiempoPoder(DURACION_PODER)
                    else:
                        self.pacman.activarPoder(DURACION_PODER)

                    for f in self.fantasmas:
                        f.setTexturaVulnerable()

                del(self.cocos[i])

                self.cocos_comidos += 1

                self.puntuacion += PUNTUACION_COCO

                if self.puntuacion > 0 and self.puntuacion % 2000 == 0:

                    self.pacman.vidas += 1
                    self.soundVidaExtra.play()

                break

        #Poderes de Pacman
        if self.pacman.poder_activo:
            self.pacman.modificarTiempoPoder(-1 //self.dificultad)
        else:
            pygame.mixer.music.stop()
            for f in self.fantasmas:
                f.setTexturaInicial()

        #Encarcelamiento y liberacion de fantasmas
        for f in self.fantasmas:
            if f.boolEnPrision:
                f.modificarTiempoPrision(-1 // self.dificultad, self.niveles[self.nivel_actual])

#Renderizar el Juego

# Renderiza todos los elementos en pantalla
# Creada en 27/06/2015
# Autor Neycker Aguayo
# Version 1.0
    def dibujarJuego(self):

        self.Ventana.fill(NEGRO)

        #Nivel
        self.niveles[self.nivel_actual].dibujar(self.Ventana)

        #Cocos
        for coco in self.cocos:
            self.Ventana.blit(coco.getSprite(), coco.posicion.toTuple())

        #Fruta
        if self.fruta != None:
            self.Ventana.blit(self.fruta.getSprite(), self.niveles[self.nivel_actual].posFruta.toTuple())


        #Fantasmas

        for i in range(0, len(self.fantasmas)):
            self.Ventana.blit(self.fantasmas[i].getSprite(), self.fantasmas[i].posicion.toTuple())

        #Pacman
        self.Ventana.blit(self.pacman.getSprite(), self.pacman.posicion.toTuple())


    def dibujarHUD(self):

        y = 30
        separacion = 30

        tiempo = self.hudFont.render("Tiempo Jugado: " + self.formatearTiempo(self.tiempo),True, BLANCO)

        punt = self.hudFont.render("Puntuacion: " + str(self.puntuacion), True, BLANCO)

        vidas = self.hudFont.render("Vidas: " + str(self.pacman.vidas), True, BLANCO)

        dificultad = self.hudFont.render("Dificultad: " + str(self.dificultad), True, BLANCO)

        self.Ventana.blit(tiempo, (ANCHO_BALDOSAS * 28 + 30, y))
        self.Ventana.blit(punt, (ANCHO_BALDOSAS * 28 + 30, y + separacion))
        self.Ventana.blit(vidas, (ANCHO_BALDOSAS * 28 + 30, y + separacion * 2))
        self.Ventana.blit(dificultad, (ANCHO_BALDOSAS * 28 + 30, y + separacion * 3))

    def formatearTiempo(self, tiempo):

        r_string = ""

        minutos = 0
        segundos = 0

        if tiempo > 0:
            minutos = tiempo//60
            segundos = tiempo % 60

            r_string += str(minutos) + ':' + str(segundos)

        return r_string


#Menues

# Crea una pantalla de inicio
# Creada en 28/06/2015
# Autor Oscar Moreno
# Version 1.0
    def pantallaInicio(self):

        self.reloj = pygame.time.Clock()
        self.Ventana = pygame.display.set_mode(
            (600, 400))

        self.Ventana.fill(NEGRO)


        title = pygame.image.load('Texturas/pacmanP.png')
        titleRect = title.get_rect()
        topCoord = 100 # topCoord tracks where to position the top of the text
        titleRect.top = topCoord
        titleRect.centerx = 300 #ANCHO_VENTANA/2
        topCoord += titleRect.height

        #lista de strings
        instructionText = ['Come todos los cocos antes que los fantasmas te atrapen',
                            'Presiona WASD para los movimientos del pacman',
                            'P para pausar el juego, ESC para salir']



        self.Ventana.blit(title, titleRect)

        for i in range(len(instructionText)):
            instSurf = self.BasicFont.render(instructionText[i], 1, BLANCO)
            instRect = instSurf.get_rect()
            topCoord += 10 # 10 pixels will go in between each line of text.
            instRect.top = topCoord
            instRect.centerx = 300 #ANCHO_VENTANA/2
            topCoord += instRect.height # Adjust for the height of the line.
            self.Ventana.blit(instSurf, instRect)

        pygame.mixer.music.load("Sound/pacman_intermission.wav")
        pygame.mixer.music.play()

        while True: # Main loop for the start screen.
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.salirJuego()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.salirJuego()
                    pygame.mixer.music.stop()
                    return # user has pressed a key, so return.

            # Display the DISPLAYSURF contents to the actual screen.
            pygame.display.update()

            self.reloj.tick()

    # def animacionNivel2(self):
    #
    #     self.Ventana.fill(NEGRO)
    #
    #     vel_pac = 2
    #
    #     an_pacman = agentes.Pacman()
    #     an_pacman.iniciar(vel_pac, Vec2(ANCHO_VENTANA + 10, ALTO_VENTANA /2  )
    #


    def text_objects(self, text, font):
        textSurface = font.render(text, True, VERDE)
        return textSurface, textSurface.get_rect()

    def win(self):
        if pygame.mixer.get_busy():
            pygame.mixer.music.stop()

        self.BasicFont = pygame.font.Font('freesansbold.ttf', 116)
        #largeText = pygame.font.SysFont("comicsansms",115)
        TextSurf, TextRect = self.text_objects("YOU WIN", self.BasicFont)
        TextRect.center = ((ANCHO_VENTANA/2),(ALTO_VENTANA/2))
        self.Ventana.blit(TextSurf, TextRect)
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.salirJuego()
            pygame.display.update()
            self.reloj.tick(15)

    def lost(self):

        self.BasicFont = pygame.font.Font('freesansbold.ttf', 116)
        #largeText = pygame.font.SysFont("comicsansms",115)
        TextSurf, TextRect = self.text_objects("You Lost", self.BasicFont)
        TextRect.center = ((ANCHO_VENTANA/2),(ALTO_VENTANA/2))
        self.Ventana.blit(TextSurf, TextRect)

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.salirJuego()
                    #pygame.quit()
                    #quit()

            #gameDisplay.fill(white)
            j = Juego()

            self.button("PLAY AGAIN",150,450,100,50,BRIGHTBLUE,AZUL,j.arrancar)
            self.button("Quit",550,450,100,50,BRIGHTRED,ROJO,self.salirJuego)
            pygame.display.update()
            self.reloj.tick(15)

            #self.pantallaInicio()


    def button(self,msg,x,y,w,h,ic,ac,action=None):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        #print(click)
        if x+w > mouse[0] > x and y+h > mouse[1] > y:
            pygame.draw.rect(self.Ventana, ac,(x,y,w,h))
            if click[0] == 1 and action != None:
                action()
        else:
            pygame.draw.rect(self.Ventana, ic,(x,y,w,h))
        smallText = pygame.font.SysFont("freesansbold.ttf",20)
        textSurf, textRect = self.text_objects(msg, smallText)
        textRect.center = ( (x+(w/2)), (y+(h/2)) )
        self.Ventana.blit(textSurf, textRect)




