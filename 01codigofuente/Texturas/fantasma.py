
import pygame
import pygame.math
pygame.init()

import colores
import nivel
from constantes import *
from proyecto_error import *


class Fantasma:


    def __init__(self):

        #float
        self.deltaCerca = 8 * ANCHO_BALDOSAS

        #Tupla (2 enteros)
        self.obtenerDireccionesHaciaElemento = None
        self.oldPosicion = None
        self.oldDireccion = None
        self.nextDireccion = None

        #Bool
        self.boolColisionPuerta = None
        self.boolEnPrision = None

        #Tipo Fantasma
        self.tipoFantasma = None

        #Elementos a reemplazar por Agente

        #tupla (2 enteros)
        self.posicion = None

        #Float
        self.velocidad = 0.0




    def iniciar(self, tFantasma):

        tipoFantasma = tFantasma
        boolColisionPuerta = True
        boolEnPrision = True

        if tipoFantasma == TIPO_FANTASMA.ROJO:
            img = pygame.image.load("Texturas/fantasmaRojo.png")
            img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))
            velocidad = 3.5
        elif tipoFantasma == TIPO_FANTASMA.AMARILLO:
            img = pygame.image.load("Texturas/fantasmaAmarillo.png")
            img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))
            velocidad = 3.0
        elif tipoFantasma == TIPO_FANTASMA.CELESTE:
            img = pygame.image.load("Texturas/fantasmaCeleste.png")
            img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))
            velocidad = 2.5
        elif tipoFantasma == TIPO_FANTASMA.ROSA:
            img = pygame.image.load("Texturas/fantasmaRosa.png")
            img = pygame.transform.scale(img, (ANCHO_BALDOSAS, ANCHO_BALDOSAS))
            velocidad = 2.0
        else:
            pygame.quit()
            crash("Error, fantasma no encontrado")

    def actualizar(self, nivel):

        x, y = nivel.posAtajo1


    def buscarPacman(self, Pacman):
        pass



    def estaPacmanCerca(self, pacman, delta):
        pass


