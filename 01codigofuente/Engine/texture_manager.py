import pygame

class TextureManager:

    id = 0

    otra = None
    def __init__(self):

        #Diccionario (path=id)
        self.id_dict = {}

        #Diccionario (id=image)
        self.sprite_dict = []


    def getId(self, texture_path):

        val = None

        if texture_path in self.id_dict:
            val = self.id_dict[texture_path]
        else:
            val = self.__class__.id
            self.id_dict[texture_path] = val

            self.sprite_dict[val] = pygame.image.load(texture_path)

            self.__class__.id += 1

        return val

    def getImage(self, id):

        if id in self.id_dict:
            return self.id_dict[id]